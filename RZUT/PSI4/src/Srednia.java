import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Srednia {
    public static Random rndm = new Random();
    PrintWriter save;
    
    public double suma;
    public double pr_walidujacy = 0.3;
    public double pr_uczacy = 1 - pr_walidujacy;
    public double sr_stypendium = 4.0;
    public int it = 0;
    public double eta = 0.05;
    public double theta = 0;
    public double localError = 0;
    public double y = 0.00;
    public int l_danych, l_wejsc;
    public double [] w;         // tab wag
    public double [][] x;       // tab danych
    public double [] outs;      // tab wyjsc
    public double [] outs2;
    public Scanner plik;
    
    public Srednia() {
        odczytPliku();
        losWag();
    }
    
    public void odczytPliku(){
        try {
            plik = new Scanner(new File("dane.txt")).useLocale(Locale.US);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Srednia.class.getName()).log(Level.SEVERE, null, ex);
        }
        l_danych = plik.nextInt();
        l_wejsc = plik.nextInt();
        x = new double[l_wejsc][l_danych];
        w = new double[l_danych];
        outs = new double[l_danych];
        outs2 = new double[l_danych];
        
        for (int i = 0; i < l_danych; i++) {
            for (int j = 0; j < l_wejsc; j++) {
                x[j][i] = plik.nextDouble();
            }
            outs[i] = plik.nextInt();
        }
    }
    
    public void losWag(){
        for (int i = 0; i < l_wejsc; i++)
            w[i]=rndm.nextDouble();
    }
    
    public void uczenie(){
        int licznik = 0;
        double MSE;
        double globalError;
        do {
            globalError=0;
            licznik++;
            for(int i=0; i<(l_danych * pr_uczacy); i++) {
                y = test(i);
                //localError = outs[i]-y;
                localError = -y;
                
                //Bez współczynnika zapominania
//                for (int j = 0; j < l_wejsc; j++) {
//                    w[j] += eta*y*x[j][i];
//                }
                
                //Ze współczynnikiem zapominania
//                for (int j = 0; j < l_wejsc; j++) {
//                    w[j] = w[j]*(1-eta) + (eta*y*x[j][i]);
//                }
                
                //Reguła Oji
                for (int j = 0; j < l_wejsc; j++) {
                    w[j] += (eta*y*(x[j][i]-y*w[j]));
                }
                
                //Reguła perceptronu
//                for (int j = 0; j < l_ocen; j++)
//                    w[j] += eta*localError*x[j][i];
//                w[l_ocen] += eta*localError;
//                globalError += localError*localError;
            }
            MSE = Math.pow((double)globalError, 2)/(double)l_danych;
            System.out.println("Iteracja: "+licznik+ " MSE = " + MSE);
        } while(licznik<1000);
        zapisz(licznik);
        for (int i = 0; i <= l_wejsc-1; i++) {
            System.out.println("w[" + i + "] = " + w[i]);
        }
    }
    
    public int test(int i) {
        double sum = 0;
        for (int j = 0; j < l_wejsc; j++)
            sum += w[j] * x[j][i];
        
        if(sum >= theta)
            return 1;
        else
            return 0;
    }
    
    public void testowanie(){
        int popr = 0;
        for(int i = (int)(l_danych*pr_uczacy); i<l_danych; i++) {
            if (test(i) == outs[i])
                popr++;
        }
        System.out.println("Poprawność: " + popr + "/" + (int)(l_danych*pr_walidujacy));
    }
    
    public void zapisz(int l){
        if(save==null){
            try {
                save = new PrintWriter(new FileWriter("iter.txt", true));
            } catch (IOException ex) {
                Logger.getLogger(Srednia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        save.append(Integer.toString(l));
        save.println();
        save.close();
    }

    public static void main(String[] args) {
        Srednia sr = new Srednia();
        sr.uczenie();
        sr.testowanie();
    }
}