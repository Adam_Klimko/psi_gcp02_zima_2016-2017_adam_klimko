import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RndmData {
    PrintWriter save, save2;
    Random los = new Random();
    final int ilosc_rekordow = 10000;
    final int ilosc_ocen = 10;
    final double srednia = 4.0;
    
    RndmData(){
        save();
        //save2();
    }
    
    void save(){
        try {
            save = new PrintWriter("dane.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RndmData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        save.println(ilosc_rekordow + " " + ilosc_ocen);
        for (int j = 0; j < ilosc_rekordow; j++) {
            for (int i = 0; i < ilosc_ocen; i++) {
                save.print((double)(los.nextInt(11)+2)/2+" ");
            }
            save.println();
        }
        save.close();
    }
    
    void save2(){
        try {
            save = new PrintWriter("dane.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RndmData.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            save2 = new PrintWriter("outs.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RndmData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        double []tab = new double[ilosc_ocen];
        double suma;
        //save.println(ilosc_rekordow + " " + ilosc_ocen);
        
        for (int j = 0; j < ilosc_rekordow; j++) {
            suma=0;
            for (int i = 0; i < ilosc_ocen; i++) {
                tab[i]=(double)(los.nextInt(11)+2)/2;
                suma+=tab[i];
                save.print(tab[i]+" ");
            }
            save.println();
            if(suma/ilosc_ocen>=srednia)
                save2.println(1);
            else
                save2.println(0);
        }
        save.close();
        save2.close();
    }
    
    public static void main(String[] args) {
        new RndmData();
    }
}
