Zajęcia 1: Neuron MCP rozwiązujący problem średniej ocen.

Zajęcia 2: Algorytm wstecznej propagacji błędów.

Zajęcia 3: Alternatywne rozwiązania dla poprzedniego problemu.

Zajęcia 4: Zaimplementowanie reguł uczenia Hebba oraz Oji.

Zajęcia 5: Grupowanie danych za pomocą SOM.