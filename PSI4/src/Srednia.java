import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Srednia {
    public static Random rndm = new Random();
    PrintWriter save;
    
    public double suma;
    public double pr_walidujacy = 0.3;
    public double pr_uczacy = 1 - pr_walidujacy;
    public double sr_stypendium = 4.0;
    public int it = 1000;
    public double eta = 0.05;
    public double theta = 0;
    public double localError = 0;
    public double y = 0.00;
    public int l_danych, l_ocen;
    public double [] w;         // tab wag
    public double [][] x;       // tab danych
    public double [] outs;      // tab wyjsc
    public double [] outs2;
    public Scanner plik;
    
    public Srednia() {
        odczytPliku();
        losWag();
    }
    
    public void odczytPliku(){
        try {
            plik = new Scanner(new File("dane.txt")).useLocale(Locale.US);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Srednia.class.getName()).log(Level.SEVERE, null, ex);
        }
        l_danych = plik.nextInt();
        l_ocen = plik.nextInt();
        x = new double[l_ocen][l_danych];
        w = new double[l_danych+1];
        outs = new double[l_danych];
        outs2 = new double[l_danych];
        
        for (int i = 0; i < l_danych; i++) {
            for (int j = 0; j < l_ocen; j++) {
                x[j][i] = plik.nextDouble();
                outs2[i] += x[j][i];
            }
            if(outs2[i] / l_ocen >= sr_stypendium)
                outs[i] = 1;
            else
                outs[i] = 0;
        }
    }
    
    public void losWag(){
        for (int i = 0; i < l_ocen+1; i++)
            w[i]=rndm.nextDouble();
    }
    
    public int licz(int p){
        suma = 0;
        for (int i = 0; i < l_ocen; i++)
            suma += x[i][p] * w[i];
        //suma += w[l_ocen];

        //Funkcja progowa unipolarna
        if(suma >= theta)
            return 1;
        else
            return 0;
    }
    
    public void uczenie(){
        int licznik = 0;
        double MSE;
        double globalError;
        do {
            globalError=0;
            licznik++;
            for(int i=0; i<(l_danych * pr_uczacy); i++) {
                y = licz(i);
                //localError = outs[i]-y;
                localError = outs[i]-y;
                
                //Bez współczynnika zapominania
//                for (int j = 0; j < l_ocen; j++) {
//                    w[j] += eta*y*x[j][i];
//                }
                
                //Ze współczynnikiem zapominania
                for (int j = 0; j < l_ocen; j++) {
                    w[j] = w[j]*(1-eta) + (eta*y*x[j][i]);
                }
                
                //Reguła Oji
//                for (int j = 0; j < l_ocen; j++) {
//                    w[j] += (eta*y*(x[j][i]-y*w[j]));
//                }
                
                //Reguła perceptronu
                //for (int j = 0; j < l_ocen; j++)
                //    w[j] += eta*localError*x[j][i];
                //w[l_ocen] += eta*localError;
                globalError += localError*localError;
            }
            MSE = Math.pow((double)globalError, 2)/(double)l_danych;
            System.out.println("Iteracja: "+licznik+ " MSE = " + MSE);
        } while(MSE>500);
        zapisz(licznik);
        for (int i = 0; i <= l_ocen-1; i++) {
            System.out.println("w[" + i + "] = " + w[i]);
        }
    }
    
    public int test(int i) {
        double apt = 0;
        for (int j = 0; j < l_ocen; j++)
            apt += w[j] * x[j][i];
        //apt += w[l_ocen];
        if(apt >= theta)
            return 1;
        else
            return 0;
    }
    
    public void testowanie(){
        int popr = 0;
        for(int i = (int)(l_danych*pr_uczacy); i<l_danych; i++) {
            if (test(i) == outs[i])
                popr++;
        }
        System.out.println("Poprawność: " + popr + "/" + (int)(l_danych*pr_walidujacy));
    }
    
    public void zapisz(int l){
        if(save==null){
            try {
                save = new PrintWriter(new FileWriter("iter.txt", true));
            } catch (IOException ex) {
                Logger.getLogger(Srednia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        save.append(Integer.toString(l));
        save.println();
        save.close();
    }

    public static void main(String[] args) {
        Srednia sr = new Srednia();
        sr.uczenie();
        sr.testowanie();
    }
}